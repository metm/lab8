package ro.ubb.movierental.core.repository;

import ro.ubb.movierental.core.domain.Movie;

public interface MovieRepository extends MovieRentalsRepository<Movie, Long>{
}
