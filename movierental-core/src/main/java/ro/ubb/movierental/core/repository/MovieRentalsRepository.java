package ro.ubb.movierental.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.ubb.movierental.core.domain.BaseEntity;

import java.io.Serializable;

public interface MovieRentalsRepository<T extends BaseEntity<ID>, ID extends
        Serializable>
        extends JpaRepository<T, ID> {
}
