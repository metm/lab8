package ro.ubb.movierental.core.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Diana
 */

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class Client extends BaseEntity<Long> implements Serializable {

    @Column(name = "firstname", nullable = false)
    private String fName;

    @Column(name = "lastname", nullable = false)
    private String lName;

    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Rent> rentals = new HashSet<>();

    public Set<Movie> getMovies() {
        rentals = rentals == null ? new HashSet<>() :
                rentals;
        return Collections.unmodifiableSet(
                this.rentals.stream().
                        map(Rent::getMovie).
                        collect(Collectors.toSet()));
    }

    public void addMovie(Movie movie) {
        Rent rental = new Rent();
        rental.setMovie(movie);
        rental.setClient(this);
        rentals.add(rental);
    }

    //public void addRents(Set<Movie> movies) {
      //  movies.forEach(this::addMovie);
    //}


    public void addRent(Movie movie, String startdate, String duedate) {
        Rent rent = new Rent();
        rent.setMovie(movie);
        rent.setStartDate(startdate);
        rent.setDueDate(duedate);
        rent.setClient(this);
        rentals.add(rent);
//        StudentDiscipline studentDiscipline = new StudentDiscipline();
//        studentDiscipline.setDiscipline(discipline);
//        studentDiscipline.setGrade(grade);
//        studentDiscipline.setStudent(this);
//        studentDisciplines.add(studentDiscipline);
    }
//    public Client(){}
//
//    public Client(String firstName,String lastName)
//    {
//        this.fName=firstName;
//        this.lName=lastName;
//    }
//
//
//    public String getfName() {
//        return fName;
//    }
//
//    public String getlName() {
//        return lName;
//    }
//
//
//    public void setfName(String fName) {
//        this.fName = fName;
//    }
//
//    public void setlName(String lName) {
//        this.lName = lName;
//    }

//    @Override
//    public String toString() {
//        return super.toString()+ fName + " "+ lName;
//    }
}
