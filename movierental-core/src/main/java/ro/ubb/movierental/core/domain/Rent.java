package ro.ubb.movierental.core.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "rent")
@IdClass(RentPK.class)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Getter
@Setter
@EqualsAndHashCode
@Builder
public class Rent implements Serializable {

    @Id
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "clientid")
    private Client client;

    @Id
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "movieid")
    private Movie movie;
    //private int movieID;
    //private int clientID;
    @Column(name = "startdate")
    private String startDate;

    @Column(name = "duedate")
    private String dueDate;

//    public Rent(){}
//
//    public Rent(int movieID, int clientID, Date startDate, Date dueDate){
//        this.movieID = movieID;
//        this.clientID = clientID;
//        this.startDate = startDate;
//        this.dueDate = dueDate;
//    }
//
//
//    public int getMovieID() {
//        return movieID;
//    }
//
//    public void setMovieID(int movieID) {
//        this.movieID = movieID;
//    }
//
//    public int getClientID() {
//        return clientID;
//    }
//
//    public void setClientID(int clientID) {
//        this.clientID = clientID;
//    }
//
//    public Date getStartDate() {
//        return startDate;
//    }
//
//    public void setStartDate(Date startDate) {
//        this.startDate = startDate;
//    }
//
//    public Date getDueDate() {
//        return dueDate;
//    }
//
//    public void setDueDate(Date dueDate) {
//        this.dueDate = dueDate;
//    }
//
    @Override
    public String toString() {

        return "Rental " + super.toString() + "\nMovieID: " + movie.getId() + "\nClientID: " + client.getId() + "\nValability: " + startDate + "->" + dueDate;
    }
}
