package ro.ubb.movierental.core.service;

import ro.ubb.movierental.core.domain.Movie;

import java.util.List;

public interface MovieService {
    List<Movie> getAllMovies();

    Movie saveMovie(Movie movie);

    Movie update(Long id, Movie convertDtoToModel);

    void delete(Long id);

    Movie findMovie(Long movieId);
}
