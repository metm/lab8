package ro.ubb.movierental.core.service;

import ro.ubb.movierental.core.domain.Client;

import java.util.List;


public interface ClientService {
    List<Client> getAllClients();

    Client saveClient(Client client);

    Client update(Long id, Client convertDtoToModel);

    void delete(Long id);

    Client findClient(Long clientId);
}
