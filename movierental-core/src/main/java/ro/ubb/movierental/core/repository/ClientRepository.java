package ro.ubb.movierental.core.repository;

import ro.ubb.movierental.core.domain.Client;

public interface ClientRepository extends MovieRentalsRepository<Client,Long> {
}
