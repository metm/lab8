package ro.ubb.movierental.core.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Diana
 */

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class Movie extends BaseEntity<Long> implements Serializable {

    @Column(name = "movieName", nullable = false)
    private String movieName;

    @Column(name = "description", nullable = false)
    private String movieDescription;

    @Column(name = "genre", nullable = false)
    private String movieGenre;

    @OneToMany(mappedBy = "movie", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Rent> rentals = new HashSet<>();

    /*public Set<Client> getClients() {
        return Collections.unmodifiableSet(
                rentals.stream()
                        .map(Rent::getClient)
                        .collect(Collectors.toSet())
        );
    }*/

    public void addClient(Client client) {
        Rent rent = new Rent();
        rent.setClient(client);
        rent.setMovie(this);
        rentals.add(rent);
    }

    public void addRent(Client client, String startdate, String duedate) {
        Rent rent = new Rent();
        rent.setMovie(this);
        rent.setClient(client);
        rent.setStartDate(startdate);
        rent.setDueDate(duedate);
//        StudentDiscipline studentDiscipline = new StudentDiscipline();
//        studentDiscipline.setStudent(student);
//        studentDiscipline.setGrade(grade);
//        studentDiscipline.setDiscipline(this);
//        studentDisciplines.add(studentDiscipline);
    }

//    public Movie(){}
//
//    public Movie(String name,String description,String genre)
//    {
//        movieName=name;
//        movieDescription=description;
//        movieGenre=genre;
//    }
//
    public String getMovieName() {
        return movieName;
    }
//
//    public String getMovieDescription() {
//        return movieDescription;
//    }
//
//    public String getMovieGenre() {
//        return movieGenre;
//    }
//
//    public void setMovieName(String movieName) {
//        this.movieName = movieName;
//    }
//
//    public void setMovieDescription(String movieDescription) {
//        this.movieDescription = movieDescription;
//    }
//
//    public void setMovieGenre(String movieGenre) {
//        this.movieGenre = movieGenre;
//    }
//
//    @Override
//    public String toString() {
//        return  super.toString()+movieName +" , Description: "+ movieDescription+", Genre: " + movieGenre;
//    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Movie movie = (Movie) o;
//        if (!movieDescription.equals(movie.movieDescription)) return false;
//        if (!movieGenre.equals(movie.movieGenre)) return false;
//        return movieName.equals(movie.movieName);
//    }
}
