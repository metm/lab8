package ro.ubb.movierental.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.movierental.core.domain.Movie;
import ro.ubb.movierental.core.repository.MovieRepository;

import java.util.List;
import java.util.Optional;

@Service
public class MovieServiceImpl implements MovieService{
    private static final Logger log = LoggerFactory.getLogger(MovieServiceImpl.class);

    @Autowired
    private MovieRepository movieRepository;

    @Override
    public List<Movie> getAllMovies() {
        log.trace("getAllMovies --- method entered");
        List<Movie> result = movieRepository.findAll();
        log.trace("getAllMovies: result={}", result);
        return result;
    }

    @Override
    public Movie saveMovie(Movie movie) {
        log.trace("saveMovie: movie={}", movie);

        Movie savedMovie = movieRepository.save(movie);
        log.trace("saveMovie --- method finished");

        return savedMovie;
    }

    @Override
    @Transactional
    public Movie update(Long id, Movie movie) {
        log.trace("update: id={},movie={}", id, movie);

        Optional<Movie> optionalMovie = movieRepository.findById(id);
        Movie result = optionalMovie.orElse(movie);
        result.setMovieName(movie.getMovieName());
        result.setMovieDescription(movie.getMovieDescription());
        result.setMovieGenre(movie.getMovieGenre());

//        movieRepository.findById(movie.getId())
//                .ifPresent(movie1-> {
//                    movie1.setMovieName(movie.getMovieName());
//                    movie1.setMovieDescription(movie.getMovieDescription());
//                    movie1.setMovieGenre(movie.getMovieGenre());
//                    log.debug("update --- movie updated? ---" + "movie={}", movie1);
//                });

        log.trace("update: result={}, result");
        return result;
    }

    @Override
    public void delete(Long id) {
        log.trace("delete: id={}", id);
        movieRepository.deleteById(id);
        log.trace("delete --- method finished");
    }

    @Override
    public Movie findMovie(Long movieId) {
        log.trace("findMovie: movieId={}", movieId);

        Movie movieOptional = movieRepository.findById(movieId).orElse(null);

        log.trace("findMovie: movieOptional={}", movieOptional);

        return movieOptional;
    }
}
