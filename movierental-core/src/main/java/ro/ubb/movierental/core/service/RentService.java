package ro.ubb.movierental.core.service;

import ro.ubb.movierental.core.domain.Rent;

public interface RentService {

    Rent getRent(Long clientID, Long movieID);
    Rent save(Rent rent);
    Rent update(Rent rent);
}
