package ro.ubb.movierental.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.movierental.core.domain.Client;
import ro.ubb.movierental.core.domain.Rent;

@Service
public class RentServiceImpl implements RentService {
    private static final Logger log = LoggerFactory.getLogger(RentServiceImpl.class);

    @Autowired
    private ClientService clientService;

    @Autowired
    private MovieService movieService;

    @Transactional
    @Override
    public Rent getRent(Long clientID, Long movieID) {
        log.trace("findRent: clientID={}, movieID={}", clientID, movieID);
        Client client = clientService.findClient(clientID);
        for (Rent r : client.getRentals()){
            if (r.getMovie().getId().equals(movieID)) {
                return r;
            }
        }

        return null;
    }

    @Transactional
    @Override
    public Rent save(Rent rent) {
        Client client = this.clientService.findClient(rent.getClient().getId());
        client.addRent(movieService.findMovie(rent.getMovie().getId()), rent.getStartDate(), rent.getDueDate());
        return null;
    }

    @Transactional
    @Override
    public Rent update(Rent rent) {
        log.trace("update: rent={}", rent);
        Client client = clientService.findClient(rent.getClient().getId());
        for(Rent r: client.getRentals()){
            if(r.getMovie().getId().equals(rent.getMovie().getId())){
                r.setStartDate(rent.getStartDate());
                r.setDueDate(rent.getDueDate());
                log.trace("update --- method finished");
                return r;
            }
        }
        return null;
    }
}
