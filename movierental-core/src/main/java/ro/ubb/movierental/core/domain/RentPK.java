package ro.ubb.movierental.core.domain;

import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class RentPK implements Serializable {
    private Client client;
    private Movie movie;
}
