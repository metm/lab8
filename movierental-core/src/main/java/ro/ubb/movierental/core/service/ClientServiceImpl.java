package ro.ubb.movierental.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.movierental.core.domain.Client;
import ro.ubb.movierental.core.repository.ClientRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ClientServiceImpl implements ClientService{
    private static final Logger log = LoggerFactory.getLogger(
            ClientServiceImpl.class);

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public List<Client> getAllClients() {
        log.trace("getAllClients --- method entered");

        List<Client> result = clientRepository.findAll();

        log.trace("getAllClients: result={}", result);

        return result;
    }

    @Override
    public Client saveClient(Client client) {
        log.trace("saveClient: client={}", client);

        Client savedClient = this.clientRepository.save(client);

//        clientRepository.save(client);

        log.trace("saveClient --- method finished");

        return savedClient;

    }

    @Override
    @Transactional
    public Client update(Long id, Client client) {
        log.trace("update: id={},client={}",id, client);

        Optional<Client> optionalClient = clientRepository.findById(id);
        Client result = optionalClient.orElse(client);
        result.setFName(client.getFName());
        result.setLName(client.getLName());
//        clientRepository.findById(client.getId())
//                .ifPresent(client1 -> {
//                    client1.setfName(client.getfName());
//                    client1.setlName(client.getlName());
//                    log.debug("update --- client updated? --- " +
//                            "client={}", client1);
//                });

        log.trace("update: result={}", result);
        return result;

    }

    @Override
    public void delete(Long id) {
        log.trace("delete: id={}", id);

        clientRepository.deleteById(id);

        log.trace("delete --- method finished");

    }

    @Override
    public Client findClient(Long clientId) {
        log.trace("findMovie: movieId={}", clientId);

        Client clientOptional = clientRepository.findById(clientId).orElse(null);

        log.trace("findClient: clientOptional={}", clientOptional);

        return clientOptional;
    }
}
