package ro.ubb.movierental.web.dto;

import lombok.*;
import ro.ubb.movierental.core.domain.Client;
import ro.ubb.movierental.core.domain.Movie;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class RentDto {
    private Long clientId;
    private Long movieId;
    private Movie movieName;
    private Client clientName;
    private String startdate;
    private String duedate;
}
