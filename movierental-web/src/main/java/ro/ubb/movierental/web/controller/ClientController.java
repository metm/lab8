package ro.ubb.movierental.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.movierental.core.domain.Client;
import ro.ubb.movierental.core.service.ClientService;
import ro.ubb.movierental.web.converter.ClientConverter;
import ro.ubb.movierental.web.dto.ClientDto;
import ro.ubb.movierental.web.dto.ClientsDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@RestController
public class ClientController {
    private static final Logger log =
            LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private ClientService clientService;

    @Autowired
    private ClientConverter clientConverter;

    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    public List<ClientDto> getClients() {
        log.trace("getAllClients --- method entered");

        List<Client> clients = clientService.getAllClients();
        Set<ClientDto> dtos = (Set<ClientDto>) clientConverter.convertModelsToDtos(clients);
        ClientsDto result = new ClientsDto(dtos);

        log.trace("getAllClients: result={}", result);
        return new ArrayList<>(dtos);
    }

    @RequestMapping(value = "/clients", method = RequestMethod.POST)
    ClientDto saveClient(@RequestBody ClientDto dto) {
        log.trace("saveClient: dto={}", dto);

        Client saved = this.clientService.saveClient(
                clientConverter.convertDtoToModel(dto)
        );
        ClientDto result = clientConverter.convertModelToDto(saved);

        log.trace("saveClient: result={}", result);

        return result;
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.PUT)
    ClientDto updateClient(@PathVariable Long id,
                             @RequestBody ClientDto dto) {
        log.trace("updateClient: id={},dto={}", id, dto);

        Client update = clientService.update(
                id,
                clientConverter.convertDtoToModel(dto));
        ClientDto result = clientConverter.convertModelToDto(update);

        return result;
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteClient(@PathVariable Long id) {
        log.trace("deleteClient: id={}", id);

        clientService.delete(id);

        log.trace("deleteClient --- method finished");

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
