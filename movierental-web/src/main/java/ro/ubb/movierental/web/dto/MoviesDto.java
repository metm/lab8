package ro.ubb.movierental.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 * author: diana
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class MoviesDto {
    private Set<MovieDto> movies;


}