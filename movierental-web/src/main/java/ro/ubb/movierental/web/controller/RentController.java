package ro.ubb.movierental.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import ro.ubb.movierental.core.domain.Rent;
import ro.ubb.movierental.core.service.ClientService;
import ro.ubb.movierental.core.service.RentService;
import ro.ubb.movierental.web.converter.RentConverter;
import ro.ubb.movierental.web.dto.RentDto;

import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
public class RentController {
    private static final Logger log =
            LoggerFactory.getLogger(RentController.class);

    @Autowired
    private ClientService clientService;

    @Autowired
    private RentConverter rentConverter;

    @Autowired
    private RentService rentService;

    @RequestMapping(value = "/rents", method = RequestMethod.GET)
    public Set<RentDto> getRents() {
        log.trace("getAllRents --- method entered");

        Set<RentDto> rentDtos = clientService.getAllClients().stream().map(client -> client.getRentals().stream().map(model -> rentConverter.convertModelToDto(model))).flatMap(Function.identity()).collect(Collectors.toSet());
        return rentDtos;
    }

    @Transactional
    @RequestMapping(value = "/rents", method = RequestMethod.POST)
    RentDto saveRent(@RequestBody RentDto dto) {
        log.trace("saveRent: dto={}", dto);

        rentService.save(rentConverter.convertDtoToModel(dto));

        //log.trace("saveRent: result={}", result);

        //return result;
        return null;
    }

    @Transactional
    @RequestMapping(value = "/rents/{clientID}/{movieID}", method = RequestMethod.PUT)
    public RentDto update(@PathVariable final Long clientID, @PathVariable Long movieID,
                           @RequestBody RentDto dto) {
        log.trace("updateRent: clientID={}, movieID={}, dto={}", clientID,movieID, dto);

        Rent rent = rentConverter.convertDtoToModel(dto);
        rentService.update(rent);


        Rent rent1 = rentService.getRent(clientID,rent.getMovie().getId());

        return rentConverter.convertModelToDto(rent1);
    }

    @Transactional
    public void updateRent(Long clientID, Long movieID, Rent rent){
        clientService.findClient(clientID).addRent(rent.getMovie(), rent.getStartDate(), rent.getDueDate());
    }

    /*@RequestMapping(value = "/clients/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteClient(@PathVariable Long id) {
        log.trace("deleteClient: id={}", id);

        clientService.delete(id);

        log.trace("deleteClient --- method finished");

        return new ResponseEntity<>(HttpStatus.OK);
    }*/

    @RequestMapping(value = "/rents/{clientID}/{movieID}", method = RequestMethod.GET)
    public RentDto getRent(@PathVariable Long clientID, @PathVariable Long movieID){
        Rent rent = rentService.getRent(clientID, movieID);
        return  rentConverter.convertModelToDto(rent);
    }

}
