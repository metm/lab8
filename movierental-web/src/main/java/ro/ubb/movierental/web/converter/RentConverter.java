package ro.ubb.movierental.web.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.ubb.movierental.core.domain.Rent;
import ro.ubb.movierental.core.service.ClientService;
import ro.ubb.movierental.core.service.MovieService;
import ro.ubb.movierental.web.dto.RentDto;

@Component
public class RentConverter extends AbstractConverter<Rent, RentDto> {

    @Autowired
    ClientService clientService;

    @Autowired
    MovieService movieService;

    @Override
    public Rent convertDtoToModel(RentDto rentDto) {

        Rent rent = Rent.builder()
                .client(clientService.findClient(rentDto.getClientId()))
                .movie(movieService.findMovie(rentDto.getMovieId()))
                .startDate(rentDto.getStartdate())
                .dueDate(rentDto.getDuedate())
                .build();
        return rent;
    }

    @Override
    public RentDto convertModelToDto(Rent rent) {
        return RentDto.builder()
                .clientId(rent.getClient().getId())
                .movieId(rent.getMovie().getId())
                .startdate(rent.getStartDate())
                .duedate(rent.getDueDate())
                .build();
    }
}
