package ro.ubb.movierental.web.converter;


import org.springframework.stereotype.Component;
import ro.ubb.movierental.core.domain.BaseEntity;
import ro.ubb.movierental.core.domain.Client;
import ro.ubb.movierental.web.dto.ClientDto;

import java.util.stream.Collectors;

/**
 * author: radu
 */
@Component
public class ClientConverter extends BaseConverter<Client, ClientDto> {
    @Override
    public Client convertDtoToModel(ClientDto dto) {
        Client client = Client.builder()
                .fName(dto.getFName())
                .lName(dto.getLName())
                .build();
        client.setId(dto.getId());
        return client;
    }

    @Override
    public ClientDto convertModelToDto(Client client) {
        ClientDto dto = ClientDto.builder()
                .fName(client.getFName())
                .lName(client.getLName())
                .movies(client.getMovies().stream()
                    .map(BaseEntity::getId).collect(Collectors.toSet()))
                .build();
        dto.setId(client.getId());
        return dto;
    }
}

