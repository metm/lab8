package ro.ubb.movierental.web.converter;

import ro.ubb.movierental.core.domain.BaseEntity;
import ro.ubb.movierental.web.dto.BaseDto;

public interface ConverterBaseEntity<Model extends BaseEntity<Long>, Dto extends BaseDto>
        extends Converter<Model, Dto>{
}
