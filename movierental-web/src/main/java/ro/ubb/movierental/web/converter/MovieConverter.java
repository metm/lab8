package ro.ubb.movierental.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.movierental.core.domain.Movie;
import ro.ubb.movierental.web.dto.MovieDto;

@Component
public class MovieConverter extends BaseConverter<Movie, MovieDto>{
    @Override
    public Movie convertDtoToModel(MovieDto dto) {
        Movie movie = Movie.builder()
                .movieName(dto.getMovieName())
                .movieDescription(dto.getDescription())
                .movieGenre(dto.getGenre())
                .build();
        movie.setId(dto.getId());
        return movie;

        /*Movie movie = Movie.builder()
                .movieName(dto.getMovieName())
                .movieDescription(dto.getDescription())
                .movieGenre(dto.getGenre())
                .build();
        movie.setId(dto.getId());
        return movie;*/
    }

    @Override
    public MovieDto convertModelToDto(Movie movie) {
        /*MovieDto dto = MovieDto.builder()
                .movieName(movie.getMovieName())
                .description(movie.getMovieDescription())
                .genre(movie.getMovieGenre())
                .build();
        dto.setId(movie.getId());
        return dto;*/
        MovieDto dto = MovieDto.builder()
                .movieName(movie.getMovieName())
                .description(movie.getMovieDescription())
                .genre(movie.getMovieGenre())
                .build();
        dto.setId(movie.getId());
        return dto;
    }
}
