package ro.ubb.movierental.web.dto;

import lombok.*;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString(callSuper = true)
@Builder
public class MovieDto extends BaseDto {
    private String movieName;
    private String description;
    private String genre;
}
