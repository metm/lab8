package ro.ubb.movierental.web.dto;

import lombok.*;

import java.util.Set;


@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Data
@ToString(callSuper = true)
@Builder
public class ClientDto extends BaseDto {
    private String fName;
    private String lName;
    private Set<Long> movies;
}