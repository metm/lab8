package ro.ubb.movierental.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.movierental.core.domain.Movie;
import ro.ubb.movierental.core.service.MovieService;
import ro.ubb.movierental.web.converter.MovieConverter;
import ro.ubb.movierental.web.dto.MovieDto;
import ro.ubb.movierental.web.dto.MoviesDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
public class MovieController {
    private static final Logger log =
            LoggerFactory.getLogger(MovieController.class);

    @Autowired
    private MovieService movieService;

    @Autowired
    private MovieConverter movieConverter;

    @RequestMapping(value = "/movies", method = RequestMethod.GET)
    public List<MovieDto> getAllMovies() {
        log.trace("getAllMovies --- method entered");

        List<Movie> movies = movieService.getAllMovies();
        Set<MovieDto> dtos = (Set<MovieDto>) movieConverter.convertModelsToDtos(movies);
        MoviesDto result = new MoviesDto(dtos);

        log.trace("getAllMovies: result={}", result);
        return new ArrayList<>(dtos);
    }

    @RequestMapping(value = "/movies", method = RequestMethod.POST)
    MovieDto saveMovie(@RequestBody MovieDto dto) {
        log.trace("saveMovie: dto={}", dto);

        Movie saved = this.movieService.saveMovie(
                movieConverter.convertDtoToModel(dto)
        );
        MovieDto result = movieConverter.convertModelToDto(saved);

        log.trace("saveMovie: result={}", result);

        return result;
    }

    @RequestMapping(value = "/movies/{id}", method = RequestMethod.PUT)
    MovieDto updateMovie(@PathVariable Long id,
                            @RequestBody MovieDto dto) {
        log.trace("updateMovie: id={},dto={}", id, dto);

        Movie update = movieService.update(
                id,
                movieConverter.convertDtoToModel(dto));
        MovieDto result = movieConverter.convertModelToDto(update);

        return result;
    }

    @RequestMapping(value = "/movies/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteMovie(@PathVariable Long id) {
        log.trace("deleteMovie: id={}", id);

        movieService.delete(id);

        log.trace("deleteMovie --- method finished");

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
