import {Component} from "@angular/core";
import {Router} from "@angular/router"

@Component({
  moduleId: module.id,
  selector: 'ubb-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css'],
})
export class MoviesComponent {

  constructor(private router: Router) {

  }

  addMovie() {
    console.log("addMovie button pressed");
    this.router.navigate(["/movie-new"]);
  }

  getMovies(){
    console.log("getMovies button pressed");
    this.router.navigate(["/movie-list"]);
  }

}
