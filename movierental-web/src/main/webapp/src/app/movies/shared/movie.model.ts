export class Movie {
  id: number;
  movieName: string;
  description: string;
  genre: string;
}
