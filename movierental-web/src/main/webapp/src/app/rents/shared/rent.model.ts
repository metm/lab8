export class Rent{
  id: number;
  movieId: number;
  clientId: number;
  startdate: string;
  duedate: string;
}
