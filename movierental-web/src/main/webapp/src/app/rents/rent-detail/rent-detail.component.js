"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
require('rxjs/add/operator/switchMap');
var RentDetailComponent = (function () {
    function RentDetailComponent(rentService, route, location) {
        this.rentService = rentService;
        this.route = route;
        this.location = location;
    }
    RentDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params
            .switchMap(function (params) { return _this.rentService.getRent(+params['clientId'], +params['movieId']); })
            .subscribe(function (rent) { return _this.rent = rent; });
    };
    RentDetailComponent.prototype.goBack = function () {
        this.location.back();
    };
    __decorate([
        core_1.Input()
    ], RentDetailComponent.prototype, "rent", void 0);
    RentDetailComponent = __decorate([
        core_1.Component({
            selector: 'ubb-rent-detail',
            templateUrl: './rent-detail.component.html',
            styleUrls: ['./rent-detail.component.css'],
        })
    ], RentDetailComponent);
    return RentDetailComponent;
}());
exports.RentDetailComponent = RentDetailComponent;
