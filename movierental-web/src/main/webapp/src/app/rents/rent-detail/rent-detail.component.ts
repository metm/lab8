import {Component, Input, OnInit} from '@angular/core';
import {RentService} from "../shared/rent.service";
import {ActivatedRoute, Params} from "@angular/router";
import {Location} from '@angular/common';
import {Rent} from "../shared/rent.model";

import 'rxjs/add/operator/switchMap';


@Component({
    selector: 'ubb-rent-detail',
    templateUrl: './rent-detail.component.html',
    styleUrls: ['./rent-detail.component.css'],
})

export class RentDetailComponent implements OnInit {

    @Input() rent: Rent;

    constructor(private rentService: RentService,
                private route: ActivatedRoute,
                private location: Location) {
    }

    ngOnInit(): void {
        this.route.params
            .switchMap((params: Params) => this.rentService.getRent(+params['clientId'], +params['movieId']))
            .subscribe(rent => this.rent = rent);
    }

    goBack(): void {
        this.location.back();
    }

    save(): void {
        this.rentService.update(this.rent)
            .subscribe(_ => this.goBack());
    }

    delete(): void{
      this.rentService.delete(this.rent.id)
        .subscribe(_=>this.goBack());
    }
}
