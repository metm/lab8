import {Component, OnInit} from '@angular/core';
import {RentService} from "../shared/rent.service";
import {Location} from "@angular/common"

@Component({
  selector: 'app-rent-new',
  templateUrl: './rent-new.component.html',
  styleUrls: ['./rent-new.component.css']
})
export class RentNewComponent implements OnInit {

  constructor(private rentService: RentService,
              private location: Location) {
  }

  ngOnInit() {
  }

  save(clientId,movieId,startdate, duedate) {
    console.log("save button pressed", clientId, movieId, startdate, duedate);

    this.rentService.save(clientId, movieId, startdate, duedate)
      .subscribe(_=>{console.debug("rent saved");
      this.location.back();},
        err => console.error("error saving rental", err));
  }

}
