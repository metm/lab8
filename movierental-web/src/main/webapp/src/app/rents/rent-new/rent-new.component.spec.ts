import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RentNewComponent } from './rent-new.component';

describe('RentNewComponent', () => {
  let component: RentNewComponent;
  let fixture: ComponentFixture<RentNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RentNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RentNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
