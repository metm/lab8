"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var RentListComponent = (function () {
    function RentListComponent(rentService, router) {
        this.rentService = rentService;
        this.router = router;
    }
    RentListComponent.prototype.ngOnInit = function () {
        this.getRents();
    };
    RentListComponent.prototype.getRents = function () {
        var _this = this;
        this.rentService.getRents()
            .subscribe(function (rents) { return _this.rents = rents; }, function (error) { return _this.errorMessage = error; });
    };
    RentListComponent.prototype.onSelect = function (rents) {
        this.selectedRent = rent;
    };
    RentListComponent.prototype.gotoDetail = function () {
        this.router.navigate(['/rent/detail', this.selectedRent.clientId, this.selectedRent.movieId]);
    };
    RentListComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'ubb-rent-list',
            templateUrl: './rent-list.component.html',
            styleUrls: ['./rent-list.component.css'],
        })
    ], RentListComponent);
    return RentListComponent;
}());
exports.RentListComponent = RentListComponent;
