import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClientsComponent} from "./clients/clients.component";
import {ClientDetailComponent} from "./clients/client-detail/client-detail.component";
import {MoviesComponent} from "./movies/movies.component";
import {ClientNewComponent} from "./clients/client-new/client-new.component";
import {ClientListComponent} from "./clients/client-list/client-list.component";
import {MovieListComponent} from "./movies/movie-list/movie-list.component";
import {MovieDetailComponent} from "./movies/movie-detail/movie-detail.component";
import {MovieNewComponent} from "./movies/movie-new/movie-new.component";
import {RentsComponent} from "./rents/rents.component";
import {RentListComponent} from "./rents/rent-list/rent-list.component";
import {RentDetailComponent} from "./rents/rent-detail/rent-detail.component";
import {RentNewComponent} from "./rents/rent-new/rent-new.component";


const routes: Routes = [
  // { path: '', redirectTo: '/home', pathMatch: 'full' },
  {path: 'clients', component: ClientsComponent},
  {path: 'client-list', component: ClientListComponent},
  {path: 'client/detail/:id', component: ClientDetailComponent},
  {path: 'client-new', component: ClientNewComponent},

  {path: 'movies', component: MoviesComponent},
  {path: 'movie-list', component: MovieListComponent},
  {path: 'movie/detail/:id', component: MovieDetailComponent},
  {path: 'movie-new', component: MovieNewComponent},

  {path: 'rents', component: RentsComponent},
  {path: 'rent-list', component: RentListComponent},
  {path: 'rent/detail/:clientId/:movieId', component: RentDetailComponent},
  {path: 'rent-new', component: RentNewComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
