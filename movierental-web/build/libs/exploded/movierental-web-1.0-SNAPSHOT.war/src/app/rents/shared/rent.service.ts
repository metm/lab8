import {Injectable} from '@angular/core';

import {HttpClient} from "@angular/common/http";

import {Rent} from "./rent.model";

import {Observable} from "rxjs";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class RentService {
  private rentsUrl = 'http://localhost:8081/api/rents';

  constructor(private httpClient: HttpClient) {
  }

  getRents(): Observable<Rent[]> {
    return this.httpClient
      .get<Array<Rent>>(this.rentsUrl);
  }

  getRent(clientId: number, movieId: number): Observable<Rent> {
    return this.getRents()
      .map(rents => rents.find(rent => rent.clientId === clientId && rent.movieId === movieId));
  }

  update(rent): Observable<Rent> {
    const url = `${this.rentsUrl}/${rent.clientId}/${rent.movieId}`;
    return this.httpClient
      .put<Rent>(url, rent);
  }

  save(clientID: number, movieID: number, startdate: string, duedate: string): Observable<Rent> {
    console.log(clientID, movieID);
    let rent = {clientID, movieID, startdate, duedate};
    console.log(rent);
    return this.httpClient
      .post<Rent>(this.rentsUrl, rent);
  }

  delete(rent: Rent | number): Observable<Rent> {
    const id = typeof rent === 'number' ? rent : rent.id;
    const url = `${this.rentsUrl}/${id}`;
    return this.httpClient
      .delete<Rent>(url);
  }

}
