import {Component} from "@angular/core";
import {Router} from "@angular/router"

@Component({
  moduleId: module.id,
  selector: 'ubb-rents',
  templateUrl: './rents.component.html',
  styleUrls: ['./rents.component.css'],
})
export class RentsComponent {

  constructor(private router: Router) {

  }

  addRent() {
    console.log("addRent button pressed");
    this.router.navigate(["/rent-new"]);
  }

  getRents(){
    console.log("getRents button pressed");
    this.router.navigate(["/rent-list"]);
  }

}
