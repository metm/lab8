import {Component, OnInit} from '@angular/core';
import {Rent} from "../shared/rent.model";
import {RentService} from "../shared/rent.service";
import {Router} from "@angular/router";


@Component({
  moduleId: module.id,
  selector: 'ubb-rent-list',
  templateUrl: './rent-list.component.html',
  styleUrls: ['./rent-list.component.css'],
})
export class RentListComponent implements OnInit {
  errorMessage: string;
  rents: Array<Rent>;
  //clients: any=[];
  selectedRent: Rent;

  constructor(private rentService: RentService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.getRents();
  }

  getRents() {
    this.rentService.getRents()
      .subscribe(
        rents => this.rents = rents,
        error => this.errorMessage = <any>error
      );
  }

  onSelect(rent: Rent): void {
    this.selectedRent = rent;
  }

  gotoDetail(): void {
    this.router.navigate(['/rent/detail', this.selectedRent.clientId, this.selectedRent.movieId]);
  }

}
