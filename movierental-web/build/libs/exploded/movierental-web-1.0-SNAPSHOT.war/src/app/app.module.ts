import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {ClientDetailComponent} from "./clients/client-detail/client-detail.component";
import {ClientsComponent} from "./clients/clients.component";
import {ClientListComponent} from "./clients/client-list/client-list.component";
import {ClientService} from "./clients/shared/client.service";
import {MoviesComponent} from './movies/movies.component';
import {MovieListComponent} from "./movies/movie-list/movie-list.component";
import {MovieService} from "./movies/shared/movie.service";
import { ClientNewComponent } from './clients/client-new/client-new.component';
import {MovieNewComponent} from "./movies/movie-new/movie-new.component";
import {MovieDetailComponent} from "./movies/movie-detail/movie-detail.component";
import {RentDetailComponent} from "./rents/rent-detail/rent-detail.component";
import {RentsComponent} from "./rents/rents.component";
import {RentListComponent} from "./rents/rent-list/rent-list.component";
import {RentNewComponent} from "./rents/rent-new/rent-new.component";
import {RentService} from "./rents/shared/rent.service";


@NgModule({
  declarations: [
    AppComponent,
    ClientDetailComponent,
    ClientsComponent,
    ClientListComponent,

    MovieDetailComponent,
    MoviesComponent,
    MovieListComponent,
    ClientNewComponent,
    MovieNewComponent,

    RentDetailComponent,
    RentsComponent,
    RentListComponent,
    RentNewComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [ClientService, MovieService, RentService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
