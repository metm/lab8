import {Injectable} from '@angular/core';

import {HttpClient} from "@angular/common/http";

import {Client} from "./client.model";

import {Observable} from "rxjs";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {C} from "@angular/core/src/render3";


@Injectable()
export class ClientService {
  private clientsUrl = 'http://localhost:8081/api/clients';

  constructor(private httpClient: HttpClient) {
  }

  getClients(): Observable<Client[]> {
    return this.httpClient
      .get<Array<Client>>(this.clientsUrl);
  }

  getClient(id: number): Observable<Client> {
    return this.getClients()
      .map(clients => clients.find(client => client.id === id));
  }

  update(client): Observable<Client> {
    const url = `${this.clientsUrl}/${client.id}`;
    return this.httpClient
      .put<Client>(url, client);
  }

  save(fname: string, lname: string): Observable<Client> {
    let client = {fname, lname};
    return this.httpClient
      .post<Client>(this.clientsUrl, client);
  }

  delete(client:Client | number): Observable<Client> {
    const id = typeof  client === 'number' ? client : client.id;
    const url = `${this.clientsUrl}/${id}`;
    return this.httpClient
      .delete<Client>(url);
  }

}
