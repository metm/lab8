import {Component, OnInit} from '@angular/core';
import {MovieService} from "../shared/movie.service";
import {Location} from "@angular/common"

@Component({
  selector: 'app-movie-new',
  templateUrl: './movie-new.component.html',
  styleUrls: ['./movie-new.component.css']
})
export class MovieNewComponent implements OnInit {

  constructor(private movieService: MovieService,
              private location: Location) {
  }

  ngOnInit() {
  }

  save(moviename, description, genre) {
    console.log("save button pressed", moviename, description, genre);

    this.movieService.save(moviename, description, genre)
      .subscribe(_ => {
          console.debug("movie saved");
          this.location.back();
        },
        err => console.error("error saving movie", err));
  }

}
